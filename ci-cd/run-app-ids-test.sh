#!/usr/bin/env bash

_host=${1}
_mag_pw=${2}
_test_run=${3}
_tr_pw=${4}
_s_token=${5}
_job_name=${6}

rand_num=$(shuf -i 10000-99999 -n 1)

_docker_image_name="app-ids_$rand_num"
_container_name="app_ids_runner_$rand_num"

#cp /root/.ssh/id_rsa .

rm -rf report.xml

cd docker-images/tests
docker build -t ${_docker_image_name}:latest .
#rm ./id_rsa

docker run -d -e "magnifier=${_host}" -e "mag_pw=${_mag_pw}" -e "tr_run_id=${_test_run}" -e "tr_pw=${_tr_pw}" -e "s_token=${_s_token}" -e "job_name=${_job_name}" --name=${_container_name} ${_docker_image_name}

dockerId=$(docker ps -aq --filter "name=${_container_name}")
docker wait $dockerId

sleep 5 

docker cp $dockerId:app/report.xml .

docker rm --force $dockerId
docker rmi --force ${_docker_image_name}
