import paramiko
from pexpect import pxssh
import re
import os
import json
import time


def create_ssh_connection(hostname, username, password):
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh_client.connect(hostname=hostname, username=username, password=password)
    except Exception as e:
        print "ERROR in connect operation"
        print (e.message)
    return ssh_client


def execute_remote_command(ssh_client, command, output_log):
    try:
        stdin, stdout, stderr = ssh_client.exec_command(command)
        write_to_file(output_log, stdout.read())
    except:
        pass


def get_validated_log(conf, file_name):
    full_log_name = os.path.join(conf["general"]["exec_dir"], file_name)

    if not os.path.isfile(full_log_name):
        raise IOError(ENOENT, "File doesn't exit.", file_name)

    print "Validating log:  " + full_log_name
    return full_log_name


def write_to_file(path, content):
    f = open(path, "wb")
    f.write(content)
    f.close()


def get_n_line_from_file(file_to_open, line_num):
    with open(file_to_open) as f:
        for i in range(1, line_num):
            f.readline()

        requested_line = f.readline()
        return requested_line


def delete_file(file_name):
    try:
        os.remove(file_name)  # deletes the file from the last day
        return True
    except Exception as e:
        print (e.message)
        return False


def generate_dict(file):
    app_id_dict = {}
    try:
        with open(file) as f:
            for line in f:
                a = line.split(", ")
                if len(a) == 5:
                    app_id_dict[a[0]] = (a[1], a[2], a[3], a[4][:-1])
    except Exception as e:
        print e
        print "Couldn't open the appidsDB"
    return app_id_dict


def load_config(json_file):
    """
    Loading json based config file
    :param json_file: The name of the file (without the full path)
    :return:
    """

    full_name = os.path.join(os.path.dirname(__file__), json_file)
    print "Loading configuration file:  " + full_name

    if (json_file is None) or (not os.path.exists(full_name)):       # Check input
        raise ValueError("Wrong input file !!")

    with open(full_name) as data_file:
        conf = json.load(data_file)

        return conf


def create_execution_dir():
    """
    Create destination directory where all execution products would be places in
    """
    new_dir_name = "Output_%s" % time.strftime("%d-%m-%Y_%H-%M-%S")
    # logging.info("Creating execution directory named:  " + new_dir_name)
    print "Creating execution directory named:  " + new_dir_name

    full_name = os.path.join(os.path.dirname(__file__), new_dir_name)

    if not os.path.exists(full_name):
        os.makedirs(full_name)
    return full_name


def get_arg_value(arg_expr):
    return str(arg_expr).split("=")[1]
