from collect_data import *
import sys
import pytest
from slackclient import SlackClient
from utils import *
from testrail import *
import datetime

LC_USER = "lc"
CONFIG_FILE_NAME = "conf.json"
today = datetime.datetime.now().strftime("%Y-%m-%d")
DB = generate_dict('appIdsDB.txt')
apps_list = DB.keys()
PORT = 22


@pytest.fixture(scope="session")
def configuration():
    configuration = load_config(CONFIG_FILE_NAME)
    configuration = prepare_config(configuration, get_arg_value(sys.argv[6]), get_arg_value(sys.argv[8]), get_arg_value(sys.argv[9]), get_arg_value(sys.argv[4]), "#app-ids-tests")
    host = get_arg_value(sys.argv[4])
    collect_version_data(host, LC_USER, get_arg_value(sys.argv[5]), configuration)
    return configuration


@pytest.fixture(scope="module")
def tr_client(configuration):
    client = APIClient(configuration["general"]["testrail_client_url"])
    client.user = configuration["general"]["testrail_client_user"]
    client.password = str(get_arg_value(sys.argv[7]))
    return client


@pytest.fixture(scope="module")
def slack_client(configuration):
    slack_client = SlackClient(configuration["general"]["slack_token"])
    return slack_client



@pytest.fixture(scope ="package", autouse=True)
def delete_old_files():
    is_deleted = delete_file('query_output.txt')
    if is_deleted is True:
        print "The recent query file was deleted successfully"
    else:
        print "Couldn't find the query file from yestarday"


@pytest.fixture(scope ="package", autouse=True)
def query(magnifier_ip, mag_pass):
    command = "mysql -B -e \"select path, src_ip, min_start_time, max_start_time from flows_agg_with_sig where min_start_time between " + "'" + today + " 00:00:00 " + "'" + "and " + "'" + today + " 23:59:59" + "'" ";\""
    connection = create_ssh_connection(magnifier_ip, LC_USER, mag_pass)
    execute_remote_command(connection, command, 'query_output.txt')
    if 'query_output.txt':
        print "New query file was created successfully"
    connection.close()


def get_test_id(app_name, APPIDICT):
    return APPIDICT[app_name][3]


def get_requested_path(app_name, APPID_DICT):
    return APPID_DICT[app_name][0]


def get_requested_source_ip(app_name, APPID_DICT):
    return APPID_DICT[app_name][1]


def pattern_search(query_file, app_name):
    pattern = ""
    res=[]
    try:
        with open(query_file, 'r') as myfile:
            data = myfile.read()
        path = get_requested_path(app_name, DB)
        source_ip = get_requested_source_ip(app_name, DB)
        if DB[app_name][2] == '1':
            pattern = path + '\s+' + source_ip + '\s+' + today + '.*'
            res = re.findall(pattern, data)
            if len(res) == 0:
                print "The expected path wasn't found. The actual path is:"
                not_expected_path = re.findall('.*' + "." + path.split(".")[-1] + '\s+' + source_ip + '\s+' + today + '.*', data)
                if len(not_expected_path)!=0:
                    for item in not_expected_path: print item
                else: print "NOT FOUND"
        else:
            pattern = '.*' + "." + path.split(".")[-1] + '\s+' + source_ip + '\s+' + today + '.*'
            res = re.findall(pattern, data)
            if len(res) == 0: print "The expected path wasn't found. The actual path is: " + '\n' + "NOT FOUND"
        return res
    except Exception as e:
        print (e.message)
        print "ERROR: while trying to open the query output"


'''
This method prints the requested log details from the magnifier's table .
'''


def handle_result(result_list, app_name, outcome, test_case, conf, tr_client, slack_client, capsys, case_id):
    for item in result_list:
        print item
    if outcome is True:
        message = app_name + " test is successfully PASSED"
    else:
        message = app_name + "test FAILED. for more info log in TESTRAIL."
    assert outcome
    add_common_to_testcase(test_case, capsys)
    set_case_result_to_testrail(tr_client, test_case, conf, outcome, case_id)
    send_slack_notification(slack_client, message)


@pytest.mark.parametrize('app_name', apps_list)
def test_check_app_id(app_name, capsys, configuration, tr_client, slack_client):
    case_id = get_test_id(app_name, DB)
    tc = prepare_test(configuration)
    print('\n' + app_name)
    result_list = pattern_search('query_output.txt', app_name)
    if len(result_list) != 0:
        outcome = True
    else:
        outcome = False
    handle_result(result_list, app_name, outcome, tc, configuration, tr_client, slack_client, capsys, case_id)


