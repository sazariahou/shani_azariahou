import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--magnifier_ip", action="store", default="35.229.56.41", help="The tested magnifier IP"
    )

    parser.addoption(
        "--mag_pass", action="store", default="unfair orbit theory", help="Magnifier Password"
    )

    parser.addoption(
        "--run_id", action="store", default="1", help="TestRail run id"
    )
    parser.addoption(
        "--tr_pass", action="store", default="2", help="TestRail password"
    )
    parser.addoption(
        "--s_token", action="store", default="3", help="Slack Token"
    )
    parser.addoption(
        "--job_name", action="store", default="4", help="Currently Executed Job Name"
    )


@pytest.fixture(scope ="package")
def magnifier_ip(request):
    return request.config.getoption("--magnifier_ip")


@pytest.fixture(scope ="package")
def mag_pass(request):
    return request.config.getoption("--mag_pass")


@pytest.fixture(scope ="package")
def run_id(request):
    return request.config.getoption("--run_id")


@pytest.fixture(scope ="package")
def tr_pass(request):
    return request.config.getoption("--tr_pass")


@pytest.fixture(scope ="package")
def s_token(request):
    return request.config.getoption("--s_token")


@pytest.fixture(scope ="package")
def job_name(request):
    return request.config.getoption("--job_name")


