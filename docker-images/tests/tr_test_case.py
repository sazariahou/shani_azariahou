'''
This module creates represents a test rail case object.
'''


import json


class tr_test_case:
    def __init__(self, version, assignedto_id):
        self.status_id = -1
        self.comment = ""
        self.version = version
        self.assignedto_id = assignedto_id

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    def add_to_comment(self, text_to_add):
        self.comment += '\n'
        self.comment += text_to_add

        return self.comment
