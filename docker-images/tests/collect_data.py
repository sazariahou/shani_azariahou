from test_helper import *


MAGNIFIER_API_USER = 'lightcyber'
MAGNIFIER_INFO_OUT = "MagnifierInfo.out"


def collect_version_data(host, user, passwd, config):
    try:
        exec_dir = os.path.join(config["general"]["exec_dir"])
        ssh_client = create_ssh_connection(host, user, passwd)

        magnifier_info_cmd = "date && less /etc/lc.conf | grep -E 'CLIENT_ID' && zgrep GIT_TAG /opt/lc/bin/lc_version.py | grep jenkins | awk '{print $3}' && pip freeze | grep malia"
        magnifier_info_out = os.path.join(exec_dir, MAGNIFIER_INFO_OUT)
        execute_remote_command(ssh_client, magnifier_info_cmd, magnifier_info_out)
        get_version(MAGNIFIER_INFO_OUT, config)     # Also setting magnifier_version key to the current config # test commit

        ssh_client.close()

    except:
        pass

