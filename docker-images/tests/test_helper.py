import json
from check_app_id_path_test import *
import pytest
from utils import *
from tr_test_case import tr_test_case


def get_version(info_log, config):
    ver_file = get_validated_log(config, info_log)
    version = get_n_line_from_file(ver_file, 3)
    malia = get_n_line_from_file(ver_file, 4)

    config["general"]["magnifier_version"] = version + " " + malia
    return version


def get_test_case(conf):
    print conf["general"]["magnifier_version"]
    tc = tr_test_case(conf["general"]["magnifier_version"], conf["general"]["testrail_assignedto_id"])
    return tc


def prepare_config(config, run_id, token, job_name, magnifier_ip, slack_channel):
    exec_dir = create_execution_dir()
    config["general"]["exec_dir"] = exec_dir
    config["general"]["magnifier_ip"] = magnifier_ip
    config["general"]["run_id"] = run_id
    config["general"]["slack_token"] = token
    config["general"]["job_name"] = job_name
    config["case_params"]["slack_channel"] = slack_channel
    return config


def prepare_test(config):
    test_case = get_test_case(config)
    return test_case


def set_case_result_to_testrail(tr_client, tc, config, test_result, case_id):
    """
    =========================   CHANGE THIS SAMPLE CODE  ! ! ! =========================
    :param tr_client:
    :param tc:
    :param config:
    :return:
    """
    if test_result:
        tc.status_id = 1
    else:
        tc.status_id = 5

    tc.version = config["general"]["magnifier_version"]

    post_url = 'add_result_for_case/{0}/{1}'.format(config["general"]["run_id"], case_id)
    tr_client.send_post(post_url, json.loads(tc.toJSON()))
    time.sleep(1)


def add_common_to_testcase(testcase, capsys):
    global captured

    captured = capsys.readouterr()
    testcase.add_to_comment(captured[0])


def send_slack_notification(slack_clnt, message):
    slack_clnt.api_call("chat.postMessage", channel="app-ids-kuku", text=message)
