
from scapy.all import *
import os
import datetime

'''
This function fire up a PCAP. It should run from a local machine every night at 03:00 AM.
The PCAPS directory contains the pcaps files and should be in the same path with this script. (Total: 19 pcaps)
'''


if __name__ == '__main__':
    sum_of_total_pcaps = 0
    counter = 0
    list_of_files = os.listdir("PCAPS")
    was_not_sent =[]
    print list_of_files
    for name in list_of_files:
        if name.endswith(".pcap"):
            sum_of_total_pcaps +=1
            pcap = "PCAPS" + '/' + name
            print name
            pkts = rdpcap(pcap)
            try:
                sendp(pkts)
                print datetime.datetime.now()
                counter +=1
                time.sleep(1) #need to change to 5 minutes
            except Exception as e:
                print (e.message)
                was_not_sent.append(name)
    if counter<sum_of_total_pcaps:
        print ("Error: The following " + str(sum_of_total_pcaps-counter) + " PCAPS were't sent")
        for error in was_not_sent:
            print error
    else:
        print str(counter) + " PCAPS were sent"




